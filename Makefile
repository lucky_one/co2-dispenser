
MCU=atmega8
F_CPU=1000000UL
CC=avr-gcc
OBJPROG=avr-objcopy

DUDE_MCU=m8
TTY:=/dev/ttyUSB0 # Linux
AVRDUDE_BAUDRATE=57600

all: program.hex clean


program.o: main.c
	$(CC) -mmcu=$(MCU) -D F_CPU=$(F_CPU) -Os $^ -o $@

program.hex: program.o
	$(OBJPROG) -j .text -j .data -O ihex program.o $@

burnusbasp: program.hex
	avrdude -p $(DUDE_MCU) -c usbasp -P usb -u -U flash:w:program.hex

burnponyser: program.hex
	avrdude -p $(DUDE_MCU) -c ponyser -P $(TTY) -u -U flash:w:program.hex \
		-b $(AVRDUDE_BAUDRATE)


clean:
	rm -f *.o *.elf
