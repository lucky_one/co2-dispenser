#include <avr/io.h>
#include <util/delay.h>

/* times given in miliseconds -> 60s = 60000ms */
#define CO2_PULSE_TIME (3*1000UL)
#define IDLE_TIME (177*1000UL)

#define MOSFET_DDR DDRB
#define MOSFET_PORT PORTB
#define MOSFET_PIN 0 //PIN_0
#define LIGHT_SENSOR_DDR DDRB
#define LIGHT_SENSOR_PINREGISTER PINB
#define LIGHT_SENSOR_PIN 1 //PIN_1
#define LIGHT_SENSOR_INDICATOR_DDR DDRB
#define LIGHT_SENSOR_INDICATOR_PORT PORTB
#define LIGHT_SENSOR_INDICATOR_PIN 2 //PIN_2

int main()
{
        MOSFET_DDR |= (1 << MOSFET_PIN);    //set mosfet pin as output
        LIGHT_SENSOR_DDR &= ~(1 << LIGHT_SENSOR_PIN);   //set light sensor pin as input
        LIGHT_SENSOR_INDICATOR_DDR |= (1 << LIGHT_SENSOR_INDICATOR_PIN);    //set light sensor led pin as output

        _delay_ms(3000UL);    //delay to allow equipment and operator preparation (before valve opening)
        for(;;) {
            if ( LIGHT_SENSOR_PINREGISTER & (1<< LIGHT_SENSOR_PIN) ) {
                LIGHT_SENSOR_INDICATOR_PORT |= (1 << LIGHT_SENSOR_INDICATOR_PIN);

                MOSFET_PORT |= (1 << MOSFET_PIN);
        		_delay_ms(CO2_PULSE_TIME);

                MOSFET_PORT &= ~(1 << MOSFET_PIN);
                _delay_ms(IDLE_TIME);
            }
            else {
                LIGHT_SENSOR_INDICATOR_PORT &= ~(1 << LIGHT_SENSOR_INDICATOR_PIN);
                MOSFET_PORT &= ~(1 << MOSFET_PIN);  //for safety
                _delay_ms(5000);  //to increase a chanse there's a delay of CO2 puLIGHT_SENSORe once light is back on
            }
        }
}
